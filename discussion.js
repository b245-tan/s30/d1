db.fruits.insertMany([
{
  name : "Apple",
  color : "Red",
  stock : 20,
  price: 40,
  supplier_id : 1,
  onSale : true,
  origin: [ "Philippines", "US" ]
},

{
  name : "Banana",
  color : "Yellow",
  stock : 15,
  price: 20,
  supplier_id : 2,
  onSale : true,
  origin: [ "Philippines", "Ecuador" ]
},

{
  name : "Kiwi",
  color : "Green",
  stock : 25,
  price: 50,
  supplier_id : 1,
  onSale : true,
  origin: [ "US", "China" ]
},

{
  name : "Mango",
  color : "Yellow",
  stock : 10,
  price: 120,
  supplier_id : 2,
  onSale : false,
  origin: [ "Philippines", "India" ]
}
]);


// [SECTION] MongoDB Aggregation
  /*
  - used to generate and perform operations to create filtered results that helps us analyze data.
  */

// Using aggregate method:
  /*
  - the "$match" is used to pass the documents that meet the specified condition/conditions to the next stage or aggregation process.

  Syntax:
  {$match: {field: value}}
  */

  db.fruits.aggregate([{$match: {onSale: true}}]);

  /*
    - The group is used to group elements together and the field value the data from the grouped element

      Syntax:
        {$group: _id: "fieldSetGroup"}
  */

  db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", totalFruits: {$sum: "$stock"}}}
    ]);

  // Mini Activity
    // First get all the fruits that are color yellow then group them into their supplier and get the available stocks

  db.fruits.aggregate([
    {$match: {color: "Yellow"}},
    {$group: {_id: "$supplier_id", totalFruits: {$sum: "$stock"}}}
    ]);


  // Field Projection with aggregation
    /*
      -$project can be used when aggregating data to include/excluse from the returned result

      Syntax: 
        {$project: {field: 1/0}}
    */

  db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", totalFruits: {$sum: "$stock"}}},
    {$project: {_id:0}}
    ]);

  // Sorting aggregated results
    /*
      -$sort can be used to change the order of the aggregated result
    */

  db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", totalFruits: {$sum: "$stock"}}},
    {$sort: {totalFruits: 1}},
    {$project: {_id:0}}
    ]);

  /*
    values in sort:
     1 - lowest to highest
     -1 - highest to lowest
  */


  // Aggregating results based on an array field
    /*
      the $unwind deconstructs an array field from a collection/field with an array value to output a result
    */

  db.fruits.aggregate([
      {$unwind: "$origin"},
      {$group: {_id: "$origin", fruits: {$sum: "$stock"}}}
    ])


// [SECTION] Other aggregate stages
  // $count

    // count all yellow fruits
    db.fruits.aggregate([
        {$match: {color: "Yellow"}},
        {$count: "Yellow fruits"}
      ])

  
  // $avg
    // gets the average value of the stock
  db.fruits.aggregate([
      {$match: {color: "Yellow"}},
      {$group: {_id: "$color", avgYellow: {$avg: "$stock"}}}
    ])


  // $min and $max
  db.fruits.aggregate([
      {$match: {color: "Yellow"}},
      {$group: {_id: "$color", lowestStock: {$min: "$stock"}}},
    ])

  db.fruits.aggregate([
      {$match: {color: "Yellow"}},
      {$group: {_id: "$color", highestStock: {$max: "$stock"}}},
    ])